# LottieDFWAndroidExample

Native Android animation using the library [Lottie from Airbnb](https://github.com/airbnb/lottie-android)

![snapshot](https://user-images.githubusercontent.com/2035397/41127919-42ea6c24-6a61-11e8-80e7-5a0583005bf3.gif)

## Overview

[Adobe After Effects](http://www.adobe.com/products/aftereffects.html) animations can, with the use of the [bodymovin](https://github.com/bodymovin/bodymovin) plugin, be exported as JSON parsed by [Lottie](https://github.com/airbnb/lottie-android), a library for Android that renders vector animation natively.

## Built With

* [Adobe After Effects](http://www.adobe.com/products/aftereffects.html)
* [bodymovin](https://github.com/bodymovin/bodymovin)
* [Lottie](https://github.com/airbnb/lottie-android)